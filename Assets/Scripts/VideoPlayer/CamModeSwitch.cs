﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class CamModeSwitch : MonoBehaviour {
 	public void DisableVR ()
	{
		UnityEngine.XR.XRSettings.enabled = false;
		//Camera.main.ResetAspect ();
	}

	public void EnableVR ()
	{
		UnityEngine.XR.XRSettings.enabled = true;
	}
}
