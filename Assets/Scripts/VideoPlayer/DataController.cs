using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Linq;

public class DataController : MonoBehaviour {
	public VideoData allVideoData;

	private string filePath = "https://s3.amazonaws.com/netc-http/360/playlist.json";
	private bool loaded = false;

	void Start () {
		StartCoroutine(LoadData());
	}
	
	public VideoData.Item[] GetCurrentData(){
		return allVideoData.items;
	}

	public IEnumerator LoadData(){
		WWW www = new WWW(filePath);
		yield return www;

		if (!string.IsNullOrEmpty(www.error))
                Debug.Log(www.error);

		string jsonString = www.text;
		allVideoData = VideoData.CreateFromJSON(jsonString);

		loaded = true;
	}

	public bool isLoaded(){
		return loaded;
	}
}