﻿//attached to main_menu gameobject
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MainMenuPosition : MonoBehaviour {
	public Camera m_MainCamera;
	public string environment = "StudioRoom";
	
	// Update is called once per frame
	void Update () {
		if (GameObject.Find(environment) == null){
    		return;
    	}

        if(CheckTouch()){//make the controller be located in the center of the view, everytime users click the background
            ChangeMenuPosition(this.gameObject);
        }
	}

	bool CheckTouch(){
        return (!EventSystem.current.IsPointerOverGameObject() && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began);
    }

    void ChangeMenuPosition(GameObject someGameObject)
    {
        float x = someGameObject.transform.position.x;
        float y = someGameObject.transform.position.y;
        float z = someGameObject.transform.position.z;
        float radius = (float)(Math.Sqrt(x*x+z*z));
        float rotatedDegree = m_MainCamera.transform.eulerAngles.y;
        float radianDegree = rotatedDegree/180*(float)Math.PI;

        someGameObject.transform.position = new Vector3(radius*(float)(Math.Sin(radianDegree)), y, radius*(float)(Math.Cos(radianDegree)));
        someGameObject.transform.eulerAngles = new Vector3(0f, rotatedDegree, 0f);
    }

}
