﻿//handler the display and event of buttons in main menu

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GoogleVR.VideoDemo;

public class MainMenuHandler : MonoBehaviour {
	public GameObject m_Playlist;
	public GameObject m_LeftRightBtns;
	public GameObject m_VideoPlayer;
	public GameObject DataController;
	public GameObject DisplayCanvas; //use the attached videoControlsManager script

	List <Button> m_OptionsButton;
	List <Text> m_Texts;
	Button m_LeftButton;
	Button m_RightButton;

	private int currentPage = 0;
	private int listCapacity = 5;


	private bool reloaded = false;

	VideoData.Item[] videosData;
	
	IEnumerator Start(){

		while(DataController.GetComponent<DataController>().isLoaded()==false)
		yield return new WaitForSeconds(0.05f);

		videosData = DataController.GetComponent<DataController>().GetCurrentData();

		m_OptionsButton = new List<Button>();
		m_Texts	  = new List<Text>();

		int tmpIndex = 0;

		foreach(Transform item in m_Playlist.transform){
			m_OptionsButton.Add(item.GetComponentInChildren(typeof(Button)) as Button);

			int anotherTmpIndex = tmpIndex;//due to unity bug
			(item.GetComponentInChildren(typeof(Button)) as Button).onClick.AddListener(delegate {OptionTaskOnClick(anotherTmpIndex);});
			tmpIndex = tmpIndex+1;

			foreach(Component subitem in item.GetComponentsInChildren(typeof(Text))){
				if((subitem as Text).text=="Length"){
					m_Texts.Add(subitem as Text);
				}
			}
		}

		m_LeftButton = (m_LeftRightBtns.GetComponentsInChildren(typeof(Button)))[0] as Button;
		m_RightButton = (m_LeftRightBtns.GetComponentsInChildren(typeof(Button)))[1] as Button;
		m_LeftButton.onClick.AddListener(delegate {LeftRightBtnsTaskOnClick("left");});
		m_RightButton.onClick.AddListener(delegate {LeftRightBtnsTaskOnClick("right");});

		refresh();
	}

	void Update(){
		if(DisplayCanvas.GetComponentInChildren<VideoControlsManager>().IsVideoReady()&&reloaded){//auto play video when open a video first time or switch streaming viedos
			DisplayCanvas.GetComponentInChildren<VideoControlsManager>().OnPlayPause();
			reloaded = false;
		}
	}

	void OptionTaskOnClick(int index){//change url and play video
		Debug.Log("index: "+index);
		int videoIndex = currentPage*listCapacity + index;
		if(m_VideoPlayer.GetComponentInChildren<GvrVideoPlayerTexture>().videoURL != videosData[videoIndex].url){
			m_VideoPlayer.GetComponentInChildren<GvrVideoPlayerTexture>().videoURL = videosData[videoIndex].url;
			DisplayCanvas.GetComponentInChildren<VideoControlsManager>().ReStart();
			reloaded = true;
		}
		else{
			DisplayCanvas.GetComponentInChildren<VideoControlsManager>().OnPlayPause();
		}
	}

	void LeftRightBtnsTaskOnClick(string message){
		if(message == "left"){
			currentPage = currentPage - 1;
		}
		else{
			currentPage = currentPage + 1;
		}

		refresh();
	}

	void refresh(){//update the text of the buttons and length field
		m_LeftButton.interactable = !(currentPage == 0);
		m_RightButton.interactable = !(((currentPage+1)*listCapacity>=videosData.Length));
		
		for(int i = 0; i < listCapacity; i++){
			if(i < (videosData.Length - listCapacity*currentPage)){
				(m_OptionsButton[i].GetComponentInChildren(typeof(Text)) as Text).text = videosData[currentPage*listCapacity+i].title;
				m_Texts[i].text = videosData[currentPage*listCapacity+i].length;
				m_OptionsButton[i].interactable = true;
			}
			else{
				(m_OptionsButton[i].GetComponentInChildren(typeof(Text)) as Text).text = "";
				m_Texts[i].text = "";
				m_OptionsButton[i].interactable = false;
			}
		}
	}
}