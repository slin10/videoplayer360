//S.Lin
/*Handle the behavior of VideoControls and MenuBar
    1. Timeout to hide menu
    2. Toggle when clicked
    3. Change the position of (menu straight to the camera) everytime menu shows up 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class SubmenuBehaviorManager : MonoBehaviour {
    public GameObject a,b;
    public GvrVideoPlayerTexture Player;
    public Camera m_MainCamera;

    private DateTime  lastestHoverTime;//last time pointer hovers on collider
    private int       waitTime = 5;
    private bool      hoverBegin = false;// true means start hovering
    private bool      videoOver = false; //set to true when video ended

    public string environment = "StudioRoom";

    void Start(){
        if (Player != null) {
            Player.Init();
        }
        a.SetActive(false);
        b.SetActive(false);
    }

    void Update(){
    	if (GameObject.Find(environment) != null){
    		return;
    	}
        if(CheckTouch()){//make the controller be located in the center of the view, everytime users click the background
            ChangeMenuPosition(a);
            ChangeMenuPosition(b);
        }

        if(!IsVideoEnded()){
            CheckHovering();
            videoOver = false;
        }
        else{
            if(a.activeSelf==false && !videoOver){
                ToggeleController();
                videoOver = true;
                ChangeMenuPosition(a);
                ChangeMenuPosition(b);
            }
        }
    }


    void ToggeleController(){
        bool active = !(a.activeSelf);
        a.SetActive(active);
        b.SetActive(active);
    }

    bool CheckTouch(){//return ture if background been touched
        bool isToggeled = false;
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began){ 
            if (!EventSystem.current.IsPointerOverGameObject ()) {
                ToggeleController();
                isToggeled = true;
                }         
            hoverBegin = false;
        }
        return isToggeled;
    }

    void CheckHovering()
    {
        if(a.activeSelf == true){
            if (!EventSystem.current.IsPointerOverGameObject()){
                if(hoverBegin && ((DateTime.Now-lastestHoverTime).TotalSeconds>waitTime)){
                    ToggeleController();
                    hoverBegin = false;
                }
                else if (hoverBegin == false){//start hovering
                    lastestHoverTime = DateTime.Now;
                    hoverBegin = true;
                }
            }
            else{//hover on munu
                hoverBegin = false;
            }
        }
    }

    void ChangeMenuPosition(GameObject someGameObject)
    {
        float x = someGameObject.transform.position.x;
        float y = someGameObject.transform.position.y;
        float z = someGameObject.transform.position.z;
        float radius = (float)(Math.Sqrt(x*x+z*z));
        float rotatedDegree = m_MainCamera.transform.eulerAngles.y;
        float radianDegree = rotatedDegree/180*(float)Math.PI;

        someGameObject.transform.position = new Vector3(radius*(float)(Math.Sin(radianDegree)), y, radius*(float)(Math.Cos(radianDegree)));
        someGameObject.transform.eulerAngles = new Vector3(0f, rotatedDegree, 0f);
    }

    private bool IsVideoEnded(){
        return (Player.PlayerState == GvrVideoPlayerTexture.VideoPlayerState.Ended);
    }
}