﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class VideoData
{
	[System.Serializable]
	public class Item 
	{
		public string title;
		public string url;
		public string length;
	}

	public Item[] items;

	public static VideoData CreateFromJSON(string jsonString){
		return JsonUtility.FromJson<VideoData> (jsonString);
	}
}